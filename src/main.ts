import { enableProdMode, importProvidersFrom } from '@angular/core';

import { GoogleLoginProvider, SocialAuthServiceConfig } from '@abacritt/angularx-social-login';
import { HttpClient, provideHttpClient } from '@angular/common/http';
import { bootstrapApplication } from '@angular/platform-browser';
import { provideAnimations } from '@angular/platform-browser/animations';
import { provideRouter } from '@angular/router';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { OAuthModule } from 'angular-oauth2-oidc';
import { AppComponent } from './app/app.component';
import { ContactListComponent } from './app/contact-list/contact-list.component';
import { HomeComponent } from './app/home/home.component';
import { JobOfferFormComponent } from './app/job-offer-form/job-offer-form.component';
import { OfferComponent } from './app/offer/offer.component';
import { ProfileComponent } from './app/profile/profile.component';
import { ProjectsComponent } from './app/projects/projects.component';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}


bootstrapApplication(AppComponent, {
  providers: [
    importProvidersFrom(OAuthModule.forRoot({
      resourceServer: {
        sendAccessToken: true,
        allowedUrls: ['https://dataenv.de', 'http://localhost:8080', 'http://localhost:4200']
      }
    })),
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '28411015439-cn7hsb6m4nr024mkta16k7337kmlbpdl.apps.googleusercontent.com'
            )
          }
        ],
        onError: (err) => {
          console.error(err);
        }
      } as SocialAuthServiceConfig,
    },
    provideRouter([
      { path: 'projects', component: ProjectsComponent, title: 'Projects' },
      { path: 'profile', component: ProfileComponent, title: 'Profile' },
      { path: 'offer', component: JobOfferFormComponent, title: 'Offer' },
      { path: 'offer/:offerId', component: OfferComponent },
      { path: 'contact-list', component: ContactListComponent },
      { path: '', component: HomeComponent, title: 'Home' }
    ]),
    provideAnimations(),
    provideHttpClient(),
    importProvidersFrom(TranslateModule.forRoot({ loader: { provide: TranslateLoader, useFactory: httpTranslateLoader, deps: [HttpClient] }})),
  ]
});


export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

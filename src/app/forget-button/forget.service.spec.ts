import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { SocialAuthService, SocialLoginModule, SocialUser } from '@abacritt/angularx-social-login';
import { TestBed } from '@angular/core/testing';
import { MockModule, MockProvider } from 'ng-mocks';
import { of } from 'rxjs';
import { ForgetService } from './forget.service';

describe('ForgetService', () => {


    it('delete sends url and header', () => {
        TestBed.configureTestingModule(
            {
                declarations: [MockModule(SocialLoginModule)],
                imports: [HttpClientTestingModule],
                providers: [MockProvider(SocialAuthService, { authState: of(user) })]
            });
        var httpTestingController = TestBed.inject(HttpTestingController);
        var forgetService = TestBed.inject(ForgetService)

        forgetService.deleteMyUserInfo().subscribe()

        const request = httpTestingController.expectOne('/api/user/me');
        expect(request.request.method).toEqual('DELETE');
        expect(request.request.headers.get('Authorization')).toEqual("Bearer token");
    });


    var user: SocialUser = {
        provider: '',
        id: '',
        email: '',
        name: '',
        photoUrl: '',
        firstName: '',
        lastName: '',
        authToken: '',
        idToken: 'token',
        authorizationCode: '',
        response: undefined
    }
});
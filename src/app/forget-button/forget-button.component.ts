import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ForgetService } from './forget.service';
import { TranslateModule } from '@ngx-translate/core';
import { MatMenuItem, MatMenuModule } from '@angular/material/menu';

@Component({
  selector: 'app-forget-button',
  standalone: true,
  imports: [CommonModule, TranslateModule, MatMenuModule],
  templateUrl: './forget-button.component.html',
  styleUrls: ['./forget-button.component.scss']
})
export class ForgetButtonComponent {

  constructor(public forgetService: ForgetService) { }

  forget(){
    this.forgetService.deleteMyUserInfo().subscribe()
  }

}

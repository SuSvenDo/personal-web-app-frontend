import { SocialAuthService } from "@abacritt/angularx-social-login";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, mergeMap } from "rxjs";

@Injectable({
    providedIn: 'root',
})
export class ForgetService {

    constructor(private http: HttpClient, private social : SocialAuthService) { }

    deleteMyUserInfo(): Observable<any> {
        return this.social.authState.pipe(mergeMap(auth => {
            var headers = new HttpHeaders().set('Authorization', `Bearer ${auth.idToken}`);
            return this.http.delete("/api/user/me", { headers: headers });
        }));
    }
}
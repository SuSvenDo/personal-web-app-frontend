import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgetButtonComponent } from './forget-button.component';
import { ForgetService } from './forget.service';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { of } from 'rxjs';


describe('ForgetButtonComponent', () => {
  let component: ForgetButtonComponent;
  let fixture: ComponentFixture<ForgetButtonComponent>;
  const forgetService = jasmine.createSpyObj('ForgetService', ['deleteMyUserInfo']);


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ForgetButtonComponent,
        TranslateTestingModule.withTranslations({ en: require('src/assets/i18n/en.json'), de: require('src/assets/i18n/de.json') })],
      providers: [{
        provide: ForgetService, useValue: forgetService
      }]
    });
    fixture = TestBed.createComponent(ForgetButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create button', () => {
    expect(component).toBeTruthy();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('#forget-button')).toBeTruthy()
    expect(compiled.querySelector('#forget-button')?.textContent).toContain("Delete All Personal Information")
  })

  it('should call user info delete on click', () => {
    forgetService.deleteMyUserInfo.and.returnValue(of())
    var button = fixture.debugElement.nativeElement.querySelector('#forget-button');
    button.click()
    expect(forgetService.deleteMyUserInfo).toHaveBeenCalled();
  })
  
});

import { NgModule } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CommentService } from './home/comment.service';
import { LanguageService } from './language/language.service';





@NgModule({
  imports: [

  ],
  providers: [CommentService, LanguageService]
})
export class AppModule { }

export function httpTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

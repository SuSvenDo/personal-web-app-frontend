import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Injectable({
    providedIn: 'root',
})
export class LanguageService {

    languages: Language[] = [{ code: 'en', name: 'English' }, { code: 'de', name: 'German' }];

    private readonly defaultLanguage = "en";

    constructor(public translate: TranslateService) { }

    init(browsersPerferredLanguage: string) {
        this.translate.addLangs(this.languages.map(lang => lang.code))
        this.translate.setDefaultLang(this.defaultLanguage)

        var preferredLanguage = this.languages.map(lang => lang.code).filter(code => browsersPerferredLanguage.startsWith(code))[0];
        if (preferredLanguage) {
            this.translate.use(preferredLanguage)
        }
    }

    getCurrentLang(): Language | undefined {
        return this.languages.filter(lang => lang.code == this.translate.currentLang)[0];
    }

    use(lang: string) {
        this.translate.use(lang)
    }
}

export interface Language {
    code: string;
    name: string;
}
import { LanguageService } from "./language.service";

describe("LanguageService", () => {

    it('should set language options english and german on init', () => {
        const translateService = jasmine.createSpyObj('TranslateService', ['use', 'addLangs', 'setDefaultLang']);
        const languageService = new LanguageService(translateService);
        languageService.init('en');
        expect(translateService.addLangs).toHaveBeenCalledWith(['en', 'de']);
    })

    it('should set defualt language english on init', () => {
        const translateService = jasmine.createSpyObj('TranslateService', ['use', 'addLangs', 'setDefaultLang']);
        const languageService = new LanguageService(translateService);
        languageService.init('de');
        expect(translateService.setDefaultLang).toHaveBeenCalledWith('en');
    })

    it('should use german if browser sets german as preferred', () => {
        const translateService = jasmine.createSpyObj('TranslateService', ['use', 'addLangs', 'setDefaultLang']);
        const languageService = new LanguageService(translateService);
        languageService.init('de');
        expect(translateService.use).toHaveBeenCalledWith('de')
    })

    it('should not set language to use if browser sets spanish as preferred', () => {
        const translateService = jasmine.createSpyObj('TranslateService', ['use', 'addLangs', 'setDefaultLang']);
        const languageService = new LanguageService(translateService);
        languageService.init('sp');
        expect(translateService.use.calls.count()).toBe(0)
    })
})
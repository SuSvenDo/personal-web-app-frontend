import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { TranslateFakeLoader, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CommentSectionComponent } from './comment-section.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { SocialAuthService } from '@abacritt/angularx-social-login';
import { CommentResponse, CommentService } from './comment.service';
import { By } from '@angular/platform-browser';

describe('CommentSectionComponent', () => {
  let component: CommentSectionComponent;
  let fixture: ComponentFixture<CommentSectionComponent>;
  var userComment: CommentResponse = { text: "Event Comment", _links: { self: { href: "http://test" } }, user: { userId: "id", firstName: "Max" } };
  const commentService = jasmine.createSpyObj('CommentService', ['delete', 'refresh'], {commentList$: of([userComment])});

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CommentSectionComponent, BrowserAnimationsModule, RouterTestingModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader
          }
        })],
      providers: [{
        provide: CommentService, useValue: commentService
      },
      { provide: SocialAuthService, useValue: { authState: of({ firstName: "Max" }) } }
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(CommentSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should send link to delete', fakeAsync(() => {
    commentService.delete.and.returnValue(of())
    expect(component).toBeTruthy();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('#deleteButton')?.textContent).toContain('delete');
    let button = fixture.debugElement.nativeElement.querySelector('#deleteButton');
    button.click();
    tick()
    expect(commentService.delete).toHaveBeenCalledWith('http://test');
  }));


});

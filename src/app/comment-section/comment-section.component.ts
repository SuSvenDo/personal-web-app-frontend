import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { SocialAuthService } from '@abacritt/angularx-social-login';
import { MatDividerModule } from '@angular/material/divider';
import { CommentService } from './comment.service';
import { CommentComponent } from '../comment/comment.component';

@Component({
  selector: 'app-comment-section',
  standalone: true,
  imports: [CommonModule,CommentComponent, MatIconModule, MatInputModule, MatDividerModule, TranslateModule, MatButtonModule, MatCardModule, ReactiveFormsModule],
  templateUrl: './comment-section.component.html',
  styleUrls: ['./comment-section.component.scss']

})
export class CommentSectionComponent {

  commentFormControl = new FormControl('');

  constructor(public commentService: CommentService, public authService: SocialAuthService) {
    commentService.refresh();
  }

  getTextFieldSize() {
    var value = this.commentFormControl.getRawValue();
    if (value != null) {
      return value.split(/\r\n|\r|\n/).length
    }
    return 1;
  }

  postComment() {
    this.commentService.postComment(this.commentFormControl.getRawValue()).subscribe();
  }

}

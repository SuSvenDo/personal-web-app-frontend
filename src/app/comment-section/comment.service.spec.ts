import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { SocialAuthService, SocialLoginModule, SocialUser } from '@abacritt/angularx-social-login';
import { TestBed } from '@angular/core/testing';
import { MockModule, MockProvider } from 'ng-mocks';
import { of } from 'rxjs';
import { CommentService } from "./comment.service";

describe('CommentService', () => {

    var user: SocialUser = {
        provider: '',
        id: '',
        email: '',
        name: '',
        photoUrl: '',
        firstName: '',
        lastName: '',
        authToken: '',
        idToken: 'token',
        authorizationCode: '',
        response: undefined
    }

    it('requests comments on instantiation', () => {
        TestBed.configureTestingModule(
            {
                declarations: [MockModule(SocialLoginModule)],
                imports: [HttpClientTestingModule],
                providers: [MockProvider(SocialAuthService, { authState: of() })]
            });
        var httpTestingController = TestBed.inject(HttpTestingController);
        var commentService = TestBed.inject(CommentService)

        const req = httpTestingController.expectOne('/api/comment');
        expect(req.request.method).toEqual('GET');
    });

    it('post sends payload and header', () => {
        TestBed.configureTestingModule(
            {
                declarations: [MockModule(SocialLoginModule)],
                imports: [HttpClientTestingModule],
                providers: [MockProvider(SocialAuthService, { authState: of(user) })]
            });
        var httpTestingController = TestBed.inject(HttpTestingController);
        var commentService = TestBed.inject(CommentService)

        commentService.postComment("I LIKE!").subscribe()

        const requests = httpTestingController.match('/api/comment');
        expect(requests[1].request.method).toEqual('POST');
        expect(requests[1].request.body).toEqual({ text: "I LIKE!" });
        expect(requests[1].request.headers.get('Authorization')).toEqual("Bearer token");
    });

    it('delete sends url and header', () => {
        TestBed.configureTestingModule(
            {
                declarations: [MockModule(SocialLoginModule)],
                imports: [HttpClientTestingModule],
                providers: [MockProvider(SocialAuthService, { authState: of(user) })]
            });
        var httpTestingController = TestBed.inject(HttpTestingController);
        var commentService = TestBed.inject(CommentService)

        commentService.delete("https://dataenv.de/api/comment/1").subscribe()

        const post = httpTestingController.expectOne('/api/comment/1');
        expect(post.request.method).toEqual('DELETE');
        expect(post.request.headers.get('Authorization')).toEqual("Bearer token");
    });

});
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslateFakeLoader, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { of } from 'rxjs';
import { CommentSectionComponent } from './comment-section.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { SocialAuthService } from '@abacritt/angularx-social-login';
import { CommentResponse, CommentService } from './comment.service';
import { By } from '@angular/platform-browser';
import { MockComponent } from 'ng-mocks';
import { CommentComponent } from '../comment/comment.component';

describe('CommentSectionComponent', () => {
  let component: CommentSectionComponent;
  let fixture: ComponentFixture<CommentSectionComponent>;
  var userComment : CommentResponse = { text: "Event Comment", _links: { self: { href: "me" }}, user: {userId: "id", firstName: "Max"}} ;
  var anonymousComment: CommentResponse = { text: "Anonymous Comment", _links: { self: { href: "me" } } };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CommentSectionComponent, BrowserAnimationsModule, RouterTestingModule, MockComponent(CommentComponent),
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader
          }
        })],
      providers: [{
        provide: CommentService, useValue: {
          commentList$: of([userComment, anonymousComment]),
          refresh: () => {}
        }
      },
      {provide: SocialAuthService, useValue: {authState: of({firstName: "Max"})}}
    ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(CommentSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should display user comment', () => {
    expect(component).toBeTruthy();
    const compiled = fixture.nativeElement as HTMLElement;
  });

});

import { SocialAuthService } from "@abacritt/angularx-social-login";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable, NgZone } from "@angular/core";
import { Observable, Subject, mergeMap } from "rxjs";

@Injectable({
    providedIn: 'root',
})
export class CommentService {

    constructor(private http: HttpClient, private ngZone: NgZone, private authService: SocialAuthService) {
        this.commentEventSource.onmessage = (ev) => this.ngZone.run(() => this.commentList$.next(this.toCommentList(ev)));
        this.commentEventSource.onerror = (ev) => { console.log("error", ev); };
        this.refresh();
    }

    commentEventSource = new EventSource("/api/comment/event");

    commentList$ = new Subject<CommentResponse[]>()

    refresh() {
        this.getComments().subscribe(comments => this.commentList$.next(comments._embedded.comments))
    }


    private toCommentList(ev: MessageEvent<any>): CommentResponse[] {
        return JSON.parse(ev.data)._embedded.comments;
    }

    postComment(text: string | null): Observable<Object> {
        return this.authService.authState.pipe(mergeMap(auth => {
            var headers = new HttpHeaders().set('Authorization', `Bearer ${auth.idToken}`);
            return this.http.post("/api/comment", { text: text }, { headers: headers });
        }));
    }

    getComments() {
        return this.http.get<CommentListResponse>("/api/comment");
    }

    delete(link: string): Observable<Object> {
        return this.authService.authState.pipe(mergeMap(auth => {
            var headers = new HttpHeaders().set('Authorization', `Bearer ${auth.idToken}`);
            console.log(new URL(link).pathname)
            return this.http.delete(new URL(link).pathname, { headers: headers });
        }));
    }
}

export interface CommentListResponse {
    _links?: any
    page?: any
    _embedded: { comments: CommentResponse[] }
    content?: CommentResponse[]
}

export interface CommentResponse {
    text: string
    _links: { self: { href: string } }
    user?: UserResponse
}

export interface UserResponse{
    userId?: string
    firstName?: string
    lastName?: string
    email?: string
    picUrl?: string
}
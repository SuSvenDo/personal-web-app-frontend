import { Injectable } from "@angular/core";
import { AuthConfig, OAuthService } from "angular-oauth2-oidc";
import { Observable, from, map } from "rxjs";

@Injectable({
    providedIn: 'root',
})
export class AuthService {

    public theme: string = "dark-theme";

    scopes$?: Observable<String[]>
    loginEvent: Promise<boolean>;

    constructor(private oauthService: OAuthService) {
        oauthService.configure(authConfig);
        this.loginEvent = oauthService.loadDiscoveryDocumentAndTryLogin()
        oauthService.setupAutomaticSilentRefresh();
        this.scopes$ = from(this.loginEvent).pipe(map(() => <String[]>this.oauthService.getGrantedScopes()))
    }

    login() {
        this.oauthService.initLoginFlow();
    }
    logoff() {
        this.oauthService.logOut()
    }
}

export const authConfig: AuthConfig = {

    issuer: 'https://dataenv.de/realms/root',

    redirectUri: window.location.origin,

    clientId: 'root',

    scope: 'openid root-project-edit roles',

    responseType: 'code',

    requireHttps: false
}
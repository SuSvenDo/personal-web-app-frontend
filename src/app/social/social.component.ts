import { GoogleSigninButtonModule, SocialAuthService } from '@abacritt/angularx-social-login';
import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-social',
  standalone: true,
  imports: [CommonModule, GoogleSigninButtonModule],
  providers: [],
  templateUrl: './social.component.html',
  styles: [
  ]
})
export class SocialComponent {

  constructor(private authService: SocialAuthService) {

  }


  ngOnInit() {
    this.authService.authState.subscribe((user) => { });
  }


  signOut(): void {
    this.authService.signOut();
  }

}

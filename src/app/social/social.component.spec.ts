import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialComponent } from './social.component';
import { GoogleSigninButtonDirective, GoogleSigninButtonModule, SocialAuthService } from '@abacritt/angularx-social-login';
import { of } from 'rxjs';
import { MockDirective, MockModule } from 'ng-mocks';

describe('SocialComponent', () => {
  let component: SocialComponent;
  let fixture: ComponentFixture<SocialComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SocialComponent, MockModule(GoogleSigninButtonModule)],
      providers: [
        {
          provide: SocialAuthService,
          useValue: {authState: of("")}
        },
      ]
    });
    fixture = TestBed.createComponent(SocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

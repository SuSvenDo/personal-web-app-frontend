import { BaseLoginProvider, SocialUser } from "@abacritt/angularx-social-login";
import { Injectable } from "@angular/core";

@Injectable({ providedIn: 'root' })
export class MyCustomLoginProvider extends BaseLoginProvider {
    
  override initialize(autoLogin?: boolean | undefined): Promise<void> {
      throw new Error("Method not implemented.");
  }
  override getLoginStatus(): Promise<SocialUser> {
      throw new Error("Method not implemented.");
  }
  override signIn(signInOptions?: object | undefined): Promise<SocialUser> {
      throw new Error("Method not implemented.");
  }
  override signOut(revoke?: boolean | undefined): Promise<void> {
      throw new Error("Method not implemented.");
  }
  public static readonly PROVIDER_ID = 'CUSTOM' as const;

  constructor(/* infinite list of dependencies*/) {}
}
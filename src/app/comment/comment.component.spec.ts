import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { CommentComponent } from './comment.component';
import { CommentResponse, CommentService, UserResponse } from '../comment-section/comment.service';
import { of } from 'rxjs';

describe('CommentComponent', () => {
  let component: CommentComponent;
  let fixture: ComponentFixture<CommentComponent>;
  var user: UserResponse = {}
  var comment: CommentResponse = {
    text: "Nice!", _links: { self: { href: "string" } },
    user: user
  };

  var commentService = jasmine.createSpyObj('CommentService',['delete']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [CommentComponent],
      providers: [{ provide: CommentService, useValue : commentService}]
    });
    fixture = TestBed.createComponent(CommentComponent);
    component = fixture.componentInstance;
  });

  it('should show text', () => {
    component.comment =comment;
    fixture.detectChanges()
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('#comment-text')?.textContent).toBe("Nice!")
  });

  it('should show username', () => {
    component.comment =comment;
    fixture.detectChanges()
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('#comment-text')?.textContent).toBe("Nice!")
  });

  it('should show delete button', () => {
    component.comment =comment;
    fixture.detectChanges()
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('#deleteButton')).toBeTruthy();
  });

  it('should call delete on click on delete button button', fakeAsync(() => {
    component.comment =comment;
    commentService.delete.and.returnValue(of())
    fixture.detectChanges()
    var button = fixture.debugElement.nativeElement.querySelector('#deleteButton');
    button.click()
    tick()
    expect(commentService.delete).toHaveBeenCalled();
  }));


});

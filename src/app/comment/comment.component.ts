import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { CommentResponse, CommentService } from '../comment-section/comment.service';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-comment',
  standalone: true,
  imports: [CommonModule, MatCardModule, MatIconModule, MatButtonModule],
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent {
  
  @Input()
  comment!: CommentResponse;

  constructor(private commentService: CommentService){}

  delete(link: string) {
    this.commentService.delete(link).subscribe();
  }
}

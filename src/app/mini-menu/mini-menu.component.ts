import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { TranslateModule } from '@ngx-translate/core';
import { AuthService } from '../auth.service';
import { LanguageService } from '../language/language.service';
import { ThemeService } from '../theme-service';

@Component({
  selector: 'app-mini-menu',
  standalone: true,
  imports: [CommonModule, MatMenuModule, TranslateModule, MatIconModule, MatButtonModule],
  templateUrl: './mini-menu.component.html',
  styles: [
  ]
})
export class MiniMenuComponent {

  constructor(public authService: AuthService, public themeService: ThemeService, public languageService: LanguageService) { }

}

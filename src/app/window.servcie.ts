import { Injectable } from "@angular/core";
import { concat, fromEvent, map, of } from "rxjs";


@Injectable({
    providedIn: 'root',
})
export class WindowService{

    width$ = concat(of(window.innerWidth), fromEvent(window, 'resize').pipe(map(() => window.innerWidth)));

}
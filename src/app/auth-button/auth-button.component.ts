import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from '../auth.service';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-auth-button',
  standalone: true,
  imports: [CommonModule, MatButtonModule],
  templateUrl: './auth-button.component.html',
  styles: [
  ]
})
export class AuthButtonComponent {

  constructor(public authService: AuthService) { }
}

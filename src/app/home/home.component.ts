import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatRippleModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { RouterModule } from '@angular/router';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CommentSectionComponent } from '../comment-section/comment-section.component';
import { ProjectSectionComponent } from '../project-section/project-section.component';
import { SocialComponent } from '../social/social.component';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [CommonModule, SocialComponent, ProjectSectionComponent, CommentSectionComponent, MatIconModule, TranslateModule, MatCardModule, RouterModule, MatRippleModule, MatButtonModule],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public translate: TranslateService) { }


  ngOnInit(): void {
  }


}



import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Component } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateFakeLoader, TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { MockComponent } from 'ng-mocks';
import { CommentSectionComponent } from '../comment-section/comment-section.component';
import { SocialComponent } from '../social/social.component';
import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  @Component({
    selector: 'app-comment-section',
    standalone: true,
    template: ``,
  })
  class TestCommentSectionComponent {}

  beforeEach(async () => {
    TestBed.overrideComponent(HomeComponent, {
      add: {
        imports: [TestCommentSectionComponent],
      },
      remove: {
        imports: [CommentSectionComponent],
      },
    });
    await TestBed.configureTestingModule({
      declarations: [MockComponent(SocialComponent), MockComponent(CommentSectionComponent)],
      imports: [HomeComponent, RouterTestingModule, BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useClass: TranslateFakeLoader
          }
        })],
      providers: [
        TranslateService
        
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

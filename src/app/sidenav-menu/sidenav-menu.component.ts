import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MatListModule } from '@angular/material/list';
import { RouterModule } from '@angular/router';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { AuthService } from '../auth.service';
import { SidenavService } from '../sidenav/sidenav.service';

@Component({
  selector: 'app-sidenav-menu',
  standalone: true,
  imports: [CommonModule, TranslateModule, CommonModule, MatListModule, RouterModule, MatIconModule, MatButtonModule],
  templateUrl: './sidenav-menu.component.html',
  styles: ['.close-button { margin: 1em;}']
})
export class SidenavMenuComponent {

  constructor(public authService: AuthService, public sidenavService: SidenavService){}


}

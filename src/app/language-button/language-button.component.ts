import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LanguageService } from '../language/language.service';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-language-button',
  standalone: true,
  imports: [CommonModule, MatMenuModule, MatIconModule, MatButtonModule],
  templateUrl: './language-button.component.html',
  styles: [
  ]
})
export class LanguageButtonComponent {

  constructor(public languageService : LanguageService){}

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMenuButtonComponent } from './user-menu-button.component';
import { MockComponent } from 'ng-mocks';
import { UserMenuComponent } from '../user-menu/user-menu.component';

describe('UserMenuButtonComponent', () => {
  let component: UserMenuButtonComponent;
  let fixture: ComponentFixture<UserMenuButtonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [UserMenuButtonComponent, MockComponent(UserMenuComponent)]
    });
    fixture = TestBed.createComponent(UserMenuButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create button', () => {
    expect(component).toBeTruthy();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('#user-menu-button')).toBeTruthy()
  })


});

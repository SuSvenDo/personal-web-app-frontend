import { Component, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenu, MatMenuModule } from '@angular/material/menu';
import { UserMenuComponent } from '../user-menu/user-menu.component';

@Component({
  selector: 'app-user-menu-button',
  standalone: true,
  imports: [CommonModule, MatButtonModule, MatIconModule, MatMenuModule, UserMenuComponent],
  templateUrl: './user-menu-button.component.html',
  styleUrls: ['./user-menu-button.component.scss']
})
export class UserMenuButtonComponent {

}

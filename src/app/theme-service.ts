import { Injectable } from "@angular/core";


@Injectable({
    providedIn: 'root',
})
export class ThemeService{

  themes: string[] = ["dark-theme","light-theme"];
  activeTheme = this.themes[0]

  nextTheme(): string {
    var nextTheme = this.themes[this.themes.indexOf(this.activeTheme) + 1]
    return nextTheme == undefined ? this.themes[0] : nextTheme;
  }

  next(){
    this.activeTheme = this.nextTheme();
  }

}
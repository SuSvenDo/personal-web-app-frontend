import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { LanguageButtonComponent } from './language-button/language-button.component';
import { LanguageService } from './language/language.service';
import { MiniMenuComponent } from './mini-menu/mini-menu.component';
import { SidenavMenuComponent } from './sidenav-menu/sidenav-menu.component';
import { SidenavService } from './sidenav/sidenav.service';
import { ThemeService } from './theme-service';
import { ToolbarService } from './toolbar.service';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ContentComponent } from './content/content.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, LanguageButtonComponent, ToolbarComponent,
    SidenavMenuComponent, MatButtonModule, TranslateModule, ContentComponent,
    MiniMenuComponent, TranslateModule, MatIconModule, MatToolbarModule, MatSidenavModule, RouterModule],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


  isOpen$ = this.sidenav.isOpen$;
  mode$ = this.sidenav.mode$;

  constructor(
    public languageService: LanguageService,
    public themeService: ThemeService,
    public sidenav: SidenavService,
    public toolbarService: ToolbarService) {
  }

  ngOnInit(): void {
    this.languageService.init(navigator.language)
  }

}





import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { TranslateModule } from '@ngx-translate/core';
import { AuthButtonComponent } from '../auth-button/auth-button.component';
import { LanguageButtonComponent } from '../language-button/language-button.component';
import { MiniMenuComponent } from '../mini-menu/mini-menu.component';
import { SidenavService } from '../sidenav/sidenav.service';
import { ThemeService } from '../theme-service';
import { ToolbarService } from '../toolbar.service';
import { UserMenuButtonComponent } from '../user-menu-button/user-menu-button.component';

@Component({
  selector: 'app-toolbar',
  standalone: true,
  imports: [CommonModule, UserMenuButtonComponent, MiniMenuComponent, AuthButtonComponent, MatButtonModule, MatToolbarModule, MatIconModule, TranslateModule, LanguageButtonComponent],
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent {

  longToolBar$ = this.toolbarService.isFullToolbarShown$;

  constructor(public sidenavService: SidenavService,
    public toolbarService: ToolbarService, public themeService: ThemeService) { 
    }

}

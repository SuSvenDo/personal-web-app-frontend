import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolbarComponent } from './toolbar.component';
import { AuthService } from '../auth.service';
import { TranslateModule, TranslateLoader, TranslateFakeLoader } from '@ngx-translate/core';
import { MockComponent, MockModule } from 'ng-mocks';
import { UserMenuButtonComponent } from '../user-menu-button/user-menu-button.component';
import { AuthButtonComponent } from '../auth-button/auth-button.component';
import { ToolbarService } from '../toolbar.service';
import { of } from 'rxjs';
import { MiniMenuComponent } from '../mini-menu/mini-menu.component';

describe('ToolbarComponent', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ToolbarComponent, MockComponent(UserMenuButtonComponent), MockComponent(AuthButtonComponent), TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useClass: TranslateFakeLoader
        }
      })],
      providers: []
    })
      .compileComponents();

    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    var button = fixture.debugElement.nativeElement.querySelector('#user-menu-button');
    expect(button).toBeTruthy();
  });
});

describe('ToolbarComponent Small', () => {
  let component: ToolbarComponent;
  let fixture: ComponentFixture<ToolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ToolbarComponent,MockComponent(MiniMenuComponent), MockComponent(UserMenuButtonComponent), MockComponent(AuthButtonComponent), TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader,
          useClass: TranslateFakeLoader
        }
      })],
      providers: [{ provide: ToolbarService, useValue: { isFullToolbarShown$: of(false) } }]
    })
      .compileComponents();

    fixture = TestBed.createComponent(ToolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should show mini menu', () => {
    expect(component).toBeTruthy();
    var button = fixture.debugElement.nativeElement.querySelector('#mini-menu-button');
    expect(button).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMenuComponent } from './user-menu.component';
import { TranslateTestingModule } from 'ngx-translate-testing';
import { MockComponent } from 'ng-mocks';
import { ForgetButtonComponent } from '../forget-button/forget-button.component';

describe('UserMenuComponent', () => {
  let component: UserMenuComponent;
  let fixture: ComponentFixture<UserMenuComponent>;


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [UserMenuComponent, MockComponent(ForgetButtonComponent),
        TranslateTestingModule.withTranslations({ en: require('src/assets/i18n/en.json'), de: require('src/assets/i18n/de.json') })
      ]
    });
    fixture = TestBed.createComponent(UserMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('#user-menu')).toBeTruthy()
  });
});

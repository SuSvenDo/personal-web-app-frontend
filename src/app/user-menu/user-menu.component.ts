import { Component, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatMenu, MatMenuModule } from '@angular/material/menu';
import { TranslateModule } from '@ngx-translate/core';
import { ForgetButtonComponent } from '../forget-button/forget-button.component';

@Component({
  selector: 'app-user-menu',
  standalone: true,
  imports: [CommonModule, MatMenuModule, TranslateModule, ForgetButtonComponent],
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent {

  @ViewChild(MatMenu, {static: true})  menu!: MatMenu;

}

import { Subject, of, reduce, take, takeLast } from "rxjs";
import { SidenavService } from "./sidenav.service";
import { WindowService } from "../window.servcie";

describe('SidenavService', () => {



    it('is closed if width is small', done => {
        var windowService = new WindowService();
        var width$ = new Subject<number>();
        windowService.width$ = width$;
        var sidenavService = new SidenavService(windowService);
        sidenavService.isOpen$?.subscribe(isOpen => { expect(isOpen).toBe(false); done() })
        width$.next(500);
    });


    it('is open if width is big', done => {
        var windowService = new WindowService();
        var width$ = new Subject<number>();
        windowService.width$ = width$;
        var sidenavService = new SidenavService(windowService);
        sidenavService.isOpen$?.subscribe(isOpen => { expect(isOpen).toBe(true); done() })
        width$.next(1201)
    });

    it('closes on close method', done => {
        var windowService = new WindowService();
        var width$ = new Subject<number>();
        windowService.width$ = width$;
        var sidenavService = new SidenavService(windowService);
        var history = sidenavService.isOpen$.pipe(take(2)).pipe(reduce<boolean, boolean[]>((acc, curr) => acc.concat(curr), []))
        history.subscribe(values => { expect(values).toEqual([true, false]); done() })
        width$.next(1201)
        sidenavService.close();
    });

});
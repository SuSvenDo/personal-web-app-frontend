import { Injectable } from "@angular/core";
import { Subject, map, merge } from "rxjs";
import { WindowService } from "../window.servcie";


@Injectable({
    providedIn: 'root',
})
export class SidenavService{
    
    constructor(private windowService: WindowService){}

    openClick$ = new Subject<boolean>();

    mode$ = this.windowService.width$.pipe(map(width => width > 600 ? 'side' : 'over'));
    wideWindow$ = this.windowService.width$.pipe(map(width => width > 1200))
    isOpen$ = merge(this.openClick$, this.wideWindow$)

    open(){
        this.openClick$.next(true)
    }

    close(){
        this.openClick$.next(false);
    }
}
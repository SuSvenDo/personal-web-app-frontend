import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { TranslateModule } from '@ngx-translate/core';
import { OAuthService } from 'angular-oauth2-oidc';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-projects',
  standalone: true,
  imports: [CommonModule, MatCardModule, MatButtonModule, TranslateModule, MatProgressBarModule],
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  frontendLink = "https://gitlab.com/SuSvenDo/personal-web-app-frontend";
  frontendPipelineUrl = "https://gitlab.com/SuSvenDo/personal-web-app-frontend/-/pipelines"
  helmFrontendLink = "https://helm.dataenv.de/api/charts/personal-web-app-frontend";
  frontendPipeline = "https://gitlab.com/SuSvenDo/personal-web-app-frontend/badges/main/pipeline.svg";
  frontendCoverageSvg = "https://gitlab.com/SuSvenDo/personal-web-app-frontend/badges/main/coverage.svg";

  frontendPipelineLoaded = false;
  backendSvgLoaded = 0;

  scopes?: String[]
  backendUrl = "https://gitlab.com/SuSvenDo/personal-web-app-backend";
  backendHelmUrl = "https://helm.dataenv.de/api/charts/personal-web-app-backend";
  backendPipelineUrl = "https://gitlab.com/SuSvenDo/personal-web-app-backend/-/pipelines"
  backendPipelineSvg = "https://gitlab.com/SuSvenDo/personal-web-app-backend/badges/main/pipeline.svg";
  backendCoverageSvg = "https://gitlab.com/SuSvenDo/personal-web-app-backend/badges/main/coverage.svg";
  backendReleaseSvg = "https://gitlab.com/SuSvenDo/personal-web-app-backend/-/badges/release.svg";
  backendReleaseUrl = "https://gitlab.com/SuSvenDo/personal-web-app-backend/-/releases";

  commentQueryUrl = "https://gitlab.com/SuSvenDo/comment-consumer"
  commentQueryPiepeline = "https://gitlab.com/SuSvenDo/personal-web-app-backend/badges/main/pipeline.svg";
  commentQueryUnitCoverage = "https://gitlab.com/SuSvenDo/comment-consumer/badges/main/coverage.svg?job=unit-test&key_text=Unit+Test+Coverage&key_width=120"
  commentQueryHelmUrl = "https://helm.dataenv.de/api/charts/comment-query";

  commentCommandUrl = "https://gitlab.com/SuSvenDo/comment-command-service"
  commentCommandPiepeline = "https://gitlab.com/SuSvenDo/comment-command-service/badges/main/pipeline.svg";
  commentCommandCoverage = "https://gitlab.com/SuSvenDo/comment-command-service/badges/main/coverage.svg?job=unit-test&key_text=Unit+Test+Coverage&key_width=120"
  commentCommandHelmUrl = "https://helm.dataenv.de/api/charts/comment-command-service";

  


  constructor(public oauthService: OAuthService, public authService: AuthService) { }

  ngOnInit(): void {
    this.authService.loginEvent.then(() => { this.scopes = <String[]>this.oauthService.getGrantedScopes() });
  }
}

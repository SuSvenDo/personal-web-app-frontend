import { concat, fromEvent, map, of } from "rxjs";
import { WindowService } from "./window.servcie";
import { Injectable } from "@angular/core";


@Injectable({
    providedIn: 'root',
})
export class ToolbarService {

    constructor(private windowService: WindowService){}

    isFullToolbarShown$ = this.windowService.width$.pipe(map(width => width > 600))
}
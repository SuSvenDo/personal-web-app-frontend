import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from 'src/environments/environment';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-content',
  standalone: true,
  imports: [CommonModule, RouterModule],
  templateUrl: './content.component.html',
  styles: ['.content {display: flex; flex-direction: column; justify-content: space-between; height: calc(100% - 4em);}']
})
export class ContentComponent {

  appVersion = environment.appVersion;

}

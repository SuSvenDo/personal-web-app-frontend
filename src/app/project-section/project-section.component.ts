import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-project-section',
  standalone: true,
  imports: [CommonModule, TranslateModule, MatCardModule, RouterModule],
  templateUrl: './project-section.component.html',
  styles: [
  ]
})
export class ProjectSectionComponent {

}

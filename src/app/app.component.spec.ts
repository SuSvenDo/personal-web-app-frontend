import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TranslateFakeLoader, TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { AppComponent } from './app.component';
import { AuthService } from './auth.service';
import { LanguageService } from './language/language.service';
import { OAuthService } from 'angular-oauth2-oidc';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponent, MockService } from 'ng-mocks';
import { UserMenuButtonComponent } from './user-menu-button/user-menu-button.component';
import { ForgetService } from './forget-button/forget.service';

describe('Welcome View', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports:
        [AppComponent, BrowserAnimationsModule, RouterTestingModule, MockComponent(UserMenuButtonComponent),
          TranslateModule.forRoot({
            loader: {
              provide: TranslateLoader,
              useClass: TranslateFakeLoader
            }
          })],

      providers: [
        LanguageService,
        { provide: AuthService, useValue: { loginEvent: new Promise(() => { }) } },
        { provide: OAuthService, useValue: {  } },
        {provide: ForgetService, useValue: MockService(ForgetService)},
        
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('should print version', () => {
    expect(component).toBeTruthy();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('#version')?.textContent).toContain('Version');
  });



});

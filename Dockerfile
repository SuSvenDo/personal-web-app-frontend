FROM nginx:alpine
COPY ./dist/root_frontend /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf
RUN cat  /etc/nginx/conf.d/default.conf
